<!-- 
    Template Name: Home
 -->

<?php get_header(); ?>
<!-- <div class="home" style="background-image:url('<?php the_field('imgbanner');?>');">
     
        
           
                <h1><?php the_field('teksi_1');?></h1>
        
</div> -->
<section class="gallery1">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="clearfix padding50"></div>
        <div class="carousel-inner">
            <?php
            $args = array(
                'post_type'  => 'banerat',
                'posts_per_page' => '-1'
            );
            $x = 0;
            $slider = new WP_Query($args);
            if ($slider->have_posts()) :
                while ($slider->have_posts()) : $slider->the_post();  ?>
            <div class="carousel-item <?php echo ($x == 0) ? 'active' : ''; ?>">
                <div class="slider-img" style="  background-image: url('<?php the_post_thumbnail_url(); ?>')">

                    <div class="container text_home">
                        <h1><?php the_title();?></h1>
                        <p><?php the_content();?></p>
                    </div>
                </div>
            </div>
            <?php
                    $x++;
                endwhile;
                wp_reset_postdata();
            endif;
            ?>
            <?php
            $count = 0;
            while ($slider->have_posts()) :
                $slider->the_post();
                ?>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="left" aria-hidden="true" id="controller">
                    <svg xmlns="http://www.w3.org/2000/svg" height="50px" viewBox="-18 -18 572.00902 572" width="50px"
                        class="">
                        <g>
                            <path
                                d="m430.292969 255.601562h-250.0625l94.164062-94.164062c4.855469-4.855469 4.855469-12.730469 0-17.582031-4.859375-4.855469-12.730469-4.855469-17.582031 0l-115.367188 115.367187c-2.347656 2.34375-3.65625 5.535156-3.621093 8.851563.023437 3.308593 1.320312 6.480469 3.621093 8.859375l115.367188 115.363281c2.335938 2.355469 5.53125 3.660156 8.851562 3.617187 3.308594-.015624 6.484376-1.3125 8.855469-3.617187 2.335938-2.332031 3.648438-5.492187 3.648438-8.792969 0-3.296875-1.3125-6.464844-3.648438-8.792968l-94.289062-94.164063h250.0625c6.890625 0 12.472656-5.582031 12.472656-12.472656 0-6.886719-5.582031-12.472657-12.472656-12.472657zm0 0"
                                data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF" />
                            <path
                                d="m268.15625-.0742188c-108.457031-.0195312-206.242188 65.3085938-247.746094 165.5117188-41.496094 100.207031-18.542968 215.542969 58.171875 292.210938 104.703125 104.703124 274.453125 104.703124 379.152344 0 104.699219-104.695313 104.699219-274.445313 0-379.148438-50.167969-50.453125-118.429687-78.746094-189.578125-78.5742188zm0 511.3554688c-134.074219 0-243.203125-109.132812-243.203125-243.207031s109.128906-243.203125 243.203125-243.203125 243.207031 109.128906 243.207031 243.203125-109.132812 243.207031-243.207031 243.207031zm0 0"
                                data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF" />
                        </g>
                    </svg>
                </span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="right" aria-hidden="true" id="controller">

                    <svg xmlns="http://www.w3.org/2000/svg" height="50px" viewBox="-18 -18 572.00902 572" width="50px"
                        class="">
                        <g>
                            <path
                                d="m279.628906 143.855469c-4.851562-4.855469-12.722656-4.855469-17.582031 0-4.855469 4.851562-4.855469 12.726562 0 17.582031l94.164063 94.164062h-250.191407c-6.886719 0-12.472656 5.585938-12.472656 12.472657 0 6.890625 5.585937 12.472656 12.472656 12.472656h250.066407l-94.164063 94.164063c-2.335937 2.328124-3.648437 5.496093-3.648437 8.792968 0 3.300782 1.3125 6.460938 3.648437 8.792969 2.335937 2.355469 5.535156 3.660156 8.855469 3.617187 3.308594-.015624 6.484375-1.3125 8.851562-3.617187l115.367188-115.363281c2.347656-2.351563 3.65625-5.542969 3.621094-8.859375-.023438-3.308594-1.320313-6.472657-3.621094-8.851563zm0 0"
                                data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF" />
                            <path
                                d="m268.15625-.0742188c-108.457031-.0195312-206.242188 65.3085938-247.746094 165.5117188-41.496094 100.207031-18.542968 215.542969 58.171875 292.210938 104.703125 104.703124 274.453125 104.703124 379.152344 0 104.699219-104.695313 104.699219-274.445313 0-379.148438-50.167969-50.453125-118.429687-78.746094-189.578125-78.5742188zm0 511.3554688c-134.074219 0-243.203125-109.132812-243.203125-243.207031s109.128906-243.203125 243.203125-243.203125 243.207031 109.128906 243.207031 243.203125-109.132812 243.207031-243.207031 243.207031zm0 0"
                                data-original="#000000" class="active-path" data-old_color="#000000" fill="#FFFFFF" />
                        </g>
                    </svg>
                </span>
                <span class="sr-only">Next</span>
            </a>
            <li data-target="#carouselExampleControls" data-slide-to="<?php echo $count; ?>"
                class="<?php echo ($count==0)? 'active':''; ?>"></li>
            <?php
                $count++;
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
</section>
<section class="kategorien" id="services">
    <div class="container ">
        <div class="row kategorien_title">
            <div class="col-12">
                <h1>UNSERE LEISTUNGEN ZUSAMMENGEFASST</h1>
                <p><?php the_field('teksti_2');?></p>
            </div>
        </div>
        <div class="kategorie">
            <div class="circle1">
            </div>
            <div class="row">
                <?php
                    $args = array(
                        'post_type' => 'postimet',
                        'order'=>'ASC',
                        'posts_per_page' => '4',
                    );
                    $loop = new WP_Query($args);
                    while ($loop->have_posts()) :
                        $loop->the_post();
                        ?>
                <div class="col-md-4 col-sm-4 col-xs-12 kategorie2">
                    <div class="adm-kategorien">
                        <a href=""><?php the_post_thumbnail(); ?></a>
                        <div class="paragraf">
                            <h3><?php the_title(); ?></h3>
                            <p><?php the_content() ?></p>
                            <a href="<?php the_field('linka');?>">Mehr</a>
                        </div>
                    </div>
                </div>
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</section>
<section class="kategorien1" id="services">
    <div class="container ">
        <div class="row kategorien_title1">
            <div class="col-12">
                <div class="b_home">
                    <h1>Uber Uns</h1>
                    <p><?php the_field('uberuns_tekst');?></p>
                </div>
            </div>
        </div>
        <div class="kategorie1">
            <div class="row">
                <?php
                    $args = array(
                        'post_type' => 'postimet1',
                        'order'=>'ASC',
                        'posts_per_page' => '1',
                    );
                    $loop = new WP_Query($args);
                    while ($loop->have_posts()) :
                        $loop->the_post();
                        ?>
                
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer();?>
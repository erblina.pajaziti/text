<!-- 
    Template Name: About
 -->
 
 <?php get_header(); ?>
 <div class="about">
 <div class="container">
     <div class="row">
        <div class="col-xs-12">
        <h1><?php the_field('heading');?></h1>
        </div>
     </div>
 </div>
 </div>
 <?php get_footer();?>